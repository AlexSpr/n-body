#include "Recorder.h"

namespace NBody {

    Recorder::Recorder(std::string& title, unsigned int width, unsigned int height, PixelFormat pixelFormat) 
        : ColorFormat(pixelFormat), Width(width), Height(height)
    {

        pixels = new unsigned char[width * height * PixelFormatSize(ColorFormat)];

        recordPath = recordPath + title + "/";
        if (pathExist(recordPath)){
            std::filesystem::remove_all("recordPath");
            std::cout << "deleting old recording of " << title << std::endl;
        }
        std::filesystem::create_directory(recordPath);
        recordPath = recordPath + imageFolder;
        std::filesystem::create_directory(recordPath);

        std::cout << recordPath << std::endl;
    }

    Recorder::~Recorder(){
        if (NULL != pixels) {
            delete[] pixels;
            pixels = NULL;
        }
    }

    bool Recorder::pathExist(const std::string &s)
    {
        struct stat buffer;
        return (stat (s.c_str(), &buffer) == 0);
    }

    void Recorder::setPixelsFromPBO(std::shared_ptr<OpenGLPixelBuffer> PixelBufferPtr){
        PixelBufferPtr->GetPixels(pixels);
        pixelRecorded = true;
    }


    void Recorder::writePixelsToFile(){
        //std::cout<<Width << " " << Height << std::endl;
        unsigned error = lodepng::encode(recordPath + std::to_string(imageCounter) + ".png", pixels, Width, Height);

        //if there's an error, display it
        if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
        imageCounter += 1;
        pixelRecorded = false;
    }
}