#ifndef BODYGEN_H
#define BODYGEN_H

#include <random>
#include <iostream>
#include <nlohmann/json.hpp>

namespace NBody {

    class bodygen{
        public:
            bodygen(nlohmann::json& config);
            bodygen(int n, int nDefined, int nStatic, nbodyvar simRadius) { this->n = n; this->nDefined = nDefined; this->nStatic = nStatic; this->simRadius = simRadius;}
            
            ~bodygen() {}
            void setSystemArrays(nbodyvar *cPtr, nbodyvar *sPtr, nbodyvar *mPtr, nbodyvar *rPtr) {this->coordinatesArray = cPtr; this->velocitiesArray = sPtr; this->massesArray = mPtr; this->rPtr = rPtr;}
            void getSystemArrays(nbodyvar *cPtr, nbodyvar *sPtr, nbodyvar *mPtr, nbodyvar *rPtr) {}
            //Generate Values for all bodies
            void generateCoordinates(nbodyvar *arrayPtr){ for (int i = nDefined; i < n; i++){ generateBodyCoordinates(arrayPtr[i*3],arrayPtr[i*3+1],arrayPtr[i*3+2]);} }
            void generateMasses(nbodyvar *mPtr, nbodyvar *cPtr){ for (int i = nDefined; i < n; i++){ generateBodyMass(mPtr[i], cPtr[i*3],cPtr[i*3+1], cPtr[i*3+2]);} }
            void generateVelocities(nbodyvar *vPtr, nbodyvar *cPtr){ for (int i = nStatic; i < n; i++){ generateBodyVelocity(vPtr[i*3],vPtr[i*3+1], vPtr[i*3+2], cPtr[i*3],cPtr[i*3+1], cPtr[i*3+2]);} }
            void generateOrbitVelocities(nbodyvar *vPtr, nbodyvar *cPtr){ for (int i = nStatic; i < n; i++){ generateBodyOrbitVelocity(vPtr[i*3],vPtr[i*3+1], vPtr[i*3+2], cPtr[i*3],cPtr[i*3+1], cPtr[i*3+2]);} }


            nbodyvar* getCoordinatesArray(){return coordinatesArray;}
            nbodyvar* getVelocitiesArray(){return velocitiesArray;}
            nbodyvar* getMassesArray(){return massesArray;}

            int getNBodies(){return n;}
            int getNStaticBodies(){return nStatic;}
            int getNDefinedBodies(){return nDefined;}
            nbodyvar getSimRadius(){return simRadius;}
            void generateBodies();
        private:
            int n = 0;              //amount of simulated bodies 
            int nDefined = 0;       //amount of already defined bodies
            int nStatic = 0;
            int orbitBodyIndex = 0;
            nbodyvar simRadius = 0;  //max coordinate limit where bodies can be created

            nbodyvar     *coordinatesArray;
            nbodyvar     *velocitiesArray;
            nbodyvar     *massesArray;
            nbodyvar     *rPtr;

            void generateBodyCoordinates(nbodyvar& x, nbodyvar& y, nbodyvar& z);   //generate x,y,z coordinates for single body
            void generateBodyMass(nbodyvar& m, nbodyvar& x, nbodyvar& y, nbodyvar& z);  //generate mass m for single body
            void generateBodyVelocity(nbodyvar& xV, nbodyvar& xY, nbodyvar& xZ, nbodyvar& x, nbodyvar& y, nbodyvar& z);    //generate velocity v for single body
            void generateBodyOrbitVelocity(nbodyvar& xV, nbodyvar& xY, nbodyvar& xZ, nbodyvar& x, nbodyvar& y, nbodyvar& z);

            nbodyvar calculateVisviva(nbodyvar& mass, nbodyvar& distance, nbodyvar& semimajorAxisLength){ return sqrt(G * mass * (2/distance - 1/semimajorAxisLength));}
            nbodyvar calculateDistance(nbodyvar& x1, nbodyvar& y1, nbodyvar& z1, nbodyvar& x2, nbodyvar& y2, nbodyvar& z2){ return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2) + pow((z2 - z1), 2));}

    };

}

#endif //BODYGEN_H