
#ifndef APPLICATION_H
#define APPLICATION_H

#include <nlohmann/json.hpp>

#include "Core.h"

#include "Window.h"
#include "physicsOCL.h"

#include "Recorder.h"

#include "Renderer/Camera.h"

#include "Platform/OpenGL/OpenGLRenderer.h"
#include "Platform/OpenGL/OpenGLShader.h"
//
#include "Platform/OpenGL/OpenGLVertexArray.h"
#include "Platform/OpenGL/OpenGLBuffer.h"

namespace NBody {

    class Application {
        public:
            Application();
            Application(char * configFile);
            ~Application();

            nlohmann::json loadConfig(char * configFile);

            void createBodies();
            void createBodiesFromConfig();
            void setupOpenGL();
            void setupSimulation();

            void setBodyColors(float* colorArray);

            void run();

        private:
            std::unique_ptr<Window> m_Window;
            std::shared_ptr<OpenGLVertexArray> m_BodyCoordinatesArray;
            std::shared_ptr<OpenGLPixelBuffer> m_pixelBuffer;
            Camera * m_Camera;
            OpenGLRenderer* m_Renderer;
            physicsOCL* simulator;

        private:
            OpenGLShader* bodyShader;
            OpenGLShader* redShader;

        private:
            int n = 0;
            int nDefined = 0;
            int nStatic = 0;
            nbodyvar simRadius = 0.0;

            int step = 0;
            int timeSteps = 0;
            float deltaTime = 0.0f;

            int totalOrbitsCounter = 0;
            int totalStepsCounter = 0;

        private:
            std::string simName = "n-body";
            std::string configPath = "Configs/";
            
            std::string configExtension = ".json";

            std::shared_ptr<Recorder> m_Recorder;

            nlohmann::json config;
            bool record = false;

            //int linearTrigRange; 

        private:
            nbodyvar* coordinatesArray; 
            nbodyvar* velocitiesArray;
            nbodyvar* massesArray;
            nbodyvar* radiusPtr;
            nbodyvar* gravityVectorArray;

    };

}
#endif //APPLICATION_H