#ifndef INFO_H
#define INFO_H
#include <CL/cl.hpp>

namespace NBody {

    class info {
        public: 
            static void printArray(double* array, int& nbody, char const* statment, int offset = 0);
            static void printVectorArray(double* array, int& nbody, char const* statment, int offset = 0);
            static void printMatrix(double* array, int& nbody, char const* statment);
            static void printVectorMatrix(double* array, int& nbody, char const* statment);
            //static void printOpenCLError(cl_int error);
            //static const char getErrorString(cl_int error);
            static void findNANinVectorArray(double* array, int& nbody, char const* statment);
            static void findNANinVectorMatrix(double* array, int& nbody, char const* statment);


            static void findVectorsInArrayConditional(double* array, int& nbody, double conditionVector[3], char const* arrayName);
            static void findVectorsInMatrixColumnConditional(double* matrix, int& size, int column, double conditionVector[3], char const* arrayName);
    };

}

#endif //INFO_H