#ifndef RECORDER_H
#define RECORDER_H

#include "Core.h"
#include "Platform/OpenGL/OpenGLBuffer.h"

#include "lodepng.h"
#include <memory>
#include <sys/stat.h>
#include <filesystem>

namespace NBody {

    class Recorder {
        public:
            Recorder(   std::string& title,
						unsigned int width,
						unsigned int height,
                        PixelFormat pixelFormat);
            ~Recorder();

            bool pathExist(const std::string &s);

            void setPixelsFromPBO(std::shared_ptr<OpenGLPixelBuffer> PixelBufferPtr);
            void writePixelsToFile();
            unsigned int getRecordedImageCount(){return imageCounter;}

            bool pixelRecorded = false;
        
        private:
            std::string recordPath = "../Recordings/";
            std::string imageFolder = "img/";
            unsigned int Width;
			unsigned int Height;
            PixelFormat ColorFormat;

            unsigned int imageCounter = 0;
            unsigned char* pixels; 
    };  

}

#endif //RECORDER_H