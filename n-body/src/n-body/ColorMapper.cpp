#include "ColorMapper.h"
#include <math.h>

namespace NBody {

    void JetMapper::generateColorForRange(const float range) {
        int index = floor(range * 256) * 3;
        Red   = colorMap[index];
        Green = colorMap[index + 1];
        Blue  = colorMap[index + 2];
    }
}

