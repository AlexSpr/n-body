//#include "nbody.h"
#ifndef BODY_H
#define BODY_H

namespace NBody {

    class body {
        public:
            body(){}
            ~body(){};

            inline void setCoordinates(nbodyvar x, nbodyvar y, nbodyvar z){this->x = x; this->y = y; this->z = z; }
            inline void setVelocity(nbodyvar vx, nbodyvar vy, nbodyvar vz){this->vx = vx; this->vy = vy; this->vz = vz; }
            inline void setMass(nbodyvar m){this->m = m;}
            inline void setRadius(nbodyvar r){this->r = r;}

            inline nbodyvar getX(){ return x; }

        private:
            nbodyvar x, y, z;
            nbodyvar vx, vy, vz;
            nbodyvar m;
            nbodyvar r;
    };

}
#endif //BODY_H