#define __CL_ENABLE_EXCEPTIONS

#include <thread>

#include <nlohmann/json.hpp>

#include <glad/glad.h>
#include <GLFW/glfw3.h>


#include "Core.h"
#include "Application.h"
#include "info.h"

#include "ColorMapper.h"
#include "bodygen.h"
#include "physicsOCL.h"
#include "Timer.h"

namespace NBody {
    

    void framebuffer_size_callback(GLFWwindow* window, int width, int height)
    {
        glViewport(0, 0, width, height);
    }

    nlohmann::json Application::loadConfig(char * configFile){
        configPath.append(configFile).append(configExtension);
        std::cout << "Loading config file: " << configPath << std::endl;

        nlohmann::json j;
        std::ifstream i(configPath);
        i >> j;
        return j;
    }
    
    Application::Application(char * configFile){

        config = loadConfig(configFile);

        //n = config["body_num"];
        //simRadius = config["gen_radius"];
        timeSteps = config["time_steps"];
        deltaTime = config["delta_time"];

        if (config.contains("record")){
            if (config["record"]){
                record = true;
            }
        }

        if (config.contains("name")){
            simName = config["name"];
        }
    }
    
    Application::Application(){

        n = 1000;
        nDefined = 5;
        nStatic = 1;
        simRadius = 1000;
        timeSteps = 100000000;
        deltaTime = 0.1;

        //linearTrigRange = ((n-1)*n)/2;

        coordinatesArray = new nbodyvar[n*3];
        velocitiesArray = new nbodyvar[n*3];
        massesArray = new nbodyvar[n];

        //SUN
        coordinatesArray[0] = 0;
        coordinatesArray[1] = 0;
        coordinatesArray[2] = 0;
        velocitiesArray[0] = 0;
        velocitiesArray[1] = 0;
        velocitiesArray[2] = 0;
        massesArray[0] = 1000;
        
        //JUPITER
        coordinatesArray[3] = 300; // 0.00541640576 * 1000
        coordinatesArray[4] = 0;
        coordinatesArray[5] = 0;
        massesArray[1] = 1;
        
        //SATURN
        coordinatesArray[6] = 600; // 0.00997704028 * 1000
        coordinatesArray[7] = 0;
        coordinatesArray[8] = 0;
        massesArray[2] = 0.28571428571;
    }

    Application::~Application(){
        std::cout << "\n!!!Simulation Complete!!!\n" << std::endl;

        glfwTerminate();

        delete [] coordinatesArray;
        delete [] velocitiesArray; 
        delete [] massesArray;

        //delete [] distanceMagnitudeArray;
        delete [] gravityVectorArray;

        delete simulator;
        //delete [] gravityArray;
    }

    void Application::createBodies(){
        bodygen generator(n, nDefined, nStatic, simRadius);
        generator.setSystemArrays(coordinatesArray, velocitiesArray, massesArray, radiusPtr);
        generator.generateBodies();
    }

    void Application::createBodiesFromConfig(){
        bodygen generator(config);
        generator.generateBodies();

        coordinatesArray = generator.getCoordinatesArray();
        velocitiesArray = generator.getVelocitiesArray();
        massesArray = generator.getMassesArray();
        n = generator.getNBodies();
        nStatic = generator.getNStaticBodies();
        nDefined = generator.getNDefinedBodies();
        simRadius = generator.getSimRadius();
    }

    void Application::setBodyColors(float* colorArray) {

        JetMapper JetMapper;

        for (int i = 0; i < n; i++){
            nbodyvar x = coordinatesArray[i * 3];
            nbodyvar y = coordinatesArray[i * 3 + 1];
            nbodyvar z = coordinatesArray[i * 3 + 2];

            float distanceFromCenter = sqrt(pow(x, 2.0f) + pow(y, 2.0f) + pow(z, 2.0f));

            distanceFromCenter =  distanceFromCenter/ simRadius;
            /*
            float r = sqrt(0.25f - pow((distanceFromCenter - .5f), 2));
            float b = sqrt(0.25f - pow((distanceFromCenter - .5f), 2));
            float g = sqrt(0.25f - pow((distanceFromCenter - .5f), 2));
            */

            JetMapper.generateColorForRange(distanceFromCenter);

            colorArray[i * 4]     = JetMapper.getRed() / 255.0f;//255.0f    / 255.0f;
            colorArray[i * 4 + 1] = JetMapper.getGreen() / 255.0f;//0.0f   / 255.0f;
            colorArray[i * 4 + 2] = JetMapper.getBlue() / 255.0f;//0.0f  / 255.0f;
            colorArray[i * 4 + 3] = 1.0f;
        }
    }

    void Application::setupOpenGL(){

        m_Window = std::unique_ptr<Window>(Window::Create(WindowProps(simName)));
        m_Camera = new Camera(45.0f, m_Window->GetAspect(), 0.01f, 100000000.0f);
        m_Camera->SetPosition({0,0,-2600});
         
        m_Renderer = new OpenGLRenderer();
        m_Renderer->SetClearColor({0.0f,0.0f,0.0f,1.0f});
        
        glfwSetFramebufferSizeCallback((GLFWwindow*)m_Window->GetWindow(), framebuffer_size_callback);
        
        m_BodyCoordinatesArray = std::make_shared<OpenGLVertexArray>();

        std::shared_ptr<OpenGLVertexBuffer> m_VertexBuffer = std::make_shared<OpenGLVertexBuffer>(coordinatesArray, sizeof(coordinatesArray)*n*3);
        BufferLayout layout = {
            {ShaderDataType::Double3, "a_Position"}
        };
        m_VertexBuffer->SetLayout(layout);
        m_BodyCoordinatesArray->AddVertexBuffer(m_VertexBuffer);

        
        float* colorArray = new float[n*4]; 

        setBodyColors(colorArray);
        
        std::shared_ptr<OpenGLVertexBuffer> m_ColorBuffer = std::make_shared<OpenGLVertexBuffer>(colorArray, sizeof(colorArray)*n*3);
        BufferLayout colorlayout = {
            {ShaderDataType::Float4, "a_Color"}
        };
        m_ColorBuffer->SetLayout(colorlayout);
        m_BodyCoordinatesArray->AddVertexBuffer(m_ColorBuffer);
        
        delete [] colorArray;


        uint32_t * bodyIndices = new uint32_t[n];
        for (int i = 0; i < n; i ++) {    
            bodyIndices[i] = (uint32_t)i;
        }
        std::shared_ptr<OpenGLIndexBuffer> m_BodyIndexBuffer = std::make_shared<OpenGLIndexBuffer>(bodyIndices, n);
        m_BodyCoordinatesArray->SetIndexBuffer(m_BodyIndexBuffer);

        bodyShader = new OpenGLShader("bodyVertexColorShader.glsl", "fragmentColorShader.glsl");
        //bodyShader = new OpenGLShader("bodyVertexShader.glsl", "fragmentWhiteShader.glsl");

        m_pixelBuffer = std::make_shared<OpenGLPixelBuffer>(m_Window->GetWidth(), m_Window->GetHeight(), PixelFormat::RGBA);
        
        m_Recorder = std::make_shared<Recorder>(simName, m_Window->GetWidth(), m_Window->GetHeight(), PixelFormat::RGBA);
    }

    void Application::setupSimulation(){
        gravityVectorArray = new nbodyvar[n*n*3];
        /*
        for (int i = 0; i < n; i ++) {    
            gravityVectorArray[3 * i * n + i * 3] = 0;
            gravityVectorArray[3 * i * n + i * 3 + 1] = 0;
            gravityVectorArray[3 * i * n + i * 3 + 2] = 0;
        }
        */
        
        std::cout << "N" << n  << std::endl;
        for (int i = 0; i < n; i ++) {
            for (int j = 0; j < n; j ++) {
                //std::cout << i <<  " " << j << std::endl;
                gravityVectorArray[3 * i * 3 + j * 3] = 1;
                gravityVectorArray[3 * i * 3 + j * 3 + 1] = 1;
                gravityVectorArray[3 * i * 3 + j * 3 + 2] = 1;
            }    
        }
        
        std::cout << "N: " << n << std::endl;
        std::cout << "N Static: " << nStatic << std::endl;
        //std::cout << "N Static: " << nStatic << std::endl;

        simulator = new physicsOCL(n, nStatic, deltaTime, (GLFWwindow *)(m_Window->GetWindow()));
        simulator->setSimulationData(coordinatesArray, gravityVectorArray, velocitiesArray, massesArray);

        auto buffer = m_BodyCoordinatesArray->GetVertexBuffers().front();
        GLuint id = (GLuint)buffer->GetID();
        simulator->setCoordinateBufferFromOpenGL(id);
        simulator->createVelocityBuffer();
        simulator->createGravityVectorBuffer();
        simulator->createMassBuffer();

        simulator->setUpCLGLPositionKernel();
        simulator->setUpVerletVelocityKernel();
        simulator->setUpCLGLGravityKernel();


        simulator->acquireOpenGLObjects();

        simulator->computeGravity();

        simulator->releaseOpenGLObjects();
    }

    void Application::run(){

        bool show = true;
        
        while (step < timeSteps && !glfwWindowShouldClose((GLFWwindow *)(m_Window->GetWindow()))) {
            {
                //Timer timer;
                //std::thread rT(&Recorder::writePixelsToFile,m_Recorder);

                simulator->acquireOpenGLObjects();

                simulator->computeVerletVelocity();

                simulator->computePositions();

                simulator->computeGravity();
                
                simulator->computeVerletVelocity();

                simulator->readGravVectorBuffer();

                simulator->releaseOpenGLObjects();

                
                //rT.join();
                //DEBUG_COMPUTE_MESSAGE("TEST")
            }

            std::cout << "!!!Step " << step << "!!!" << std::endl;
            if (show){
                double cond[3] = {1.0f, 1.0f, 1.0f};
                //info::findVectorsInArrayConditional(gravityVectorArray, n, cond, "Gravity Vector");
                //std::cout << 
                //int values = 1;
                //info::printVectorArray(gravityVectorArray, values, "GravityVector for Jupiter", 3);
                info::findVectorsInMatrixColumnConditional(gravityVectorArray, n, 0, cond, "Gravity Vector");
                //show = false;
            }
            
            

            //std::cout << m_Recorder->getRecordedImageCount() << std::endl;

            m_Renderer->Clear();

            m_Camera->ProcessInput((GLFWwindow *)(m_Window->GetWindow()));
            
            //m_Camera->SetPosition({0,0,-2600});

            m_Renderer->SetCamera(m_Camera);

            ///=glm::mat4 transform = m_Camera->m_ModelMatrix;

            m_Renderer->Submit(bodyShader, m_BodyCoordinatesArray);

            glDrawArrays(GL_POINTS, nDefined, n); //GL_POINTS
            /*
            m_Renderer->Submit(redShader, m_BodyCoordinatesArray);

            glDrawArrays(GL_POINTS, 0, nDefined); //GL_POINTS
            */
            if (record){
                m_pixelBuffer->ReadPixels();

                m_Recorder->setPixelsFromPBO(m_pixelBuffer);
            }
            
            
            m_Window->OnUpdate();

            //m_pixelBuffer->Bind();
            
            //m_pixelBuffer->Unbind();
            //glfwSwapBuffers(renderer->window);
            //glfwPollEvents();
            step += 1;
        }
    }
}

int main(int argc, char** argv){
    NBody::Application* app;
    if (argc == 2){
        app = new NBody::Application(argv[1]);
        app->createBodiesFromConfig();
    }
    else {
        app = new NBody::Application();
        app->createBodies();
    }
    app->setupOpenGL();
    app->setupSimulation();
    app->run();
    delete app;
    return 0;
}


