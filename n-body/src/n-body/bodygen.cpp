#include "Core.h"
#include "bodygen.h"
#include <iostream>

namespace NBody {

    bodygen::bodygen(nlohmann::json& config){
        n = config["body_num"];
        simRadius = config["gen_radius"];

        coordinatesArray = new nbodyvar[n*3];
        velocitiesArray = new nbodyvar[n*3];
        massesArray = new nbodyvar[n];

        
        if (config.contains("defined bodies")){
            nlohmann::json definedBodies = config["defined bodies"];
            nDefined = definedBodies.size();
            
            for (nlohmann::json::iterator it = definedBodies.begin(); it != definedBodies.end(); ++it){
                if (it.value().contains("static"))
                    if (it.value()["static"])
                        nStatic += 1;
            }
            int nStaticStartIndex = 0;
            int nDefinedStartIndex  = nStatic;

            int * bodyIndexPtr = 0;
            int arrIndex = 0;
            for (nlohmann::json::iterator it = definedBodies.begin(); it != definedBodies.end(); ++it){
            
                if (it.value().contains("static")){
                    if (it.value()["static"]){ 
                        bodyIndexPtr = &nStaticStartIndex;
                    }
                    else {
                        bodyIndexPtr = &nDefinedStartIndex;
                    }
                }

                arrIndex = 0;
                for (auto& x : it.value()["position"].items())
                {
                    coordinatesArray[*bodyIndexPtr * 3 + arrIndex] = (nbodyvar)x.value();
                    arrIndex += 1;
                }
                arrIndex = 0;
                for (auto& x : it.value()["velocity"].items())
                {
                    velocitiesArray[*bodyIndexPtr * 3 + arrIndex] = (nbodyvar)x.value();
                    arrIndex += 1;
                }

                massesArray[*bodyIndexPtr] = (nbodyvar)it.value()["mass"];

                *bodyIndexPtr += 1;

            }
        }

    }

    void bodygen::generateBodyCoordinates(nbodyvar& x, nbodyvar& y, nbodyvar& z){
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(0, 1);

        nbodyvar t = dist(e2) * PI * 2;
        nbodyvar u = dist(e2) + dist(e2);
        nbodyvar r = 0;
        
        if (u>1.0) r = 2 - u;
        else r = u;
        x = r * cos(t);
        y = r * sin(t);
        
        nbodyvar center_dist = sqrt(pow(x,2) + pow(y,2));
        nbodyvar heightPossibility = sqrt( sin(pow(center_dist,4)) - pow(center_dist,2)*sin(pow(center_dist,4)) ) / sqrt(5); 
        std::uniform_real_distribution<> uni(-heightPossibility,heightPossibility);  
        z = uni(e2);

        x *= simRadius;
        y *= simRadius;
        z *= simRadius; 
    }

    void bodygen::generateBodyMass(nbodyvar& m, nbodyvar& x, nbodyvar& y, nbodyvar& z){
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(MASS_MIN, MASS_MAX);

        m = dist(e2);
    }

    void bodygen::generateBodyVelocity(nbodyvar& xV, nbodyvar& xY, nbodyvar& xZ, nbodyvar& x, nbodyvar& y, nbodyvar& z){
        std::random_device rd;
        std::mt19937 e2(rd());
        std::uniform_real_distribution<> dist(-V_MAX, V_MAX);

        xV = dist(e2) * V_BOOST;
        xY = dist(e2) * V_BOOST;
        xZ = dist(e2) * V_BOOST;
    }

    void bodygen::generateBodyOrbitVelocity(nbodyvar& xV, nbodyvar& yV, nbodyvar& zV, nbodyvar& x, nbodyvar& y, nbodyvar& z){
        nbodyvar& orbitBodyX = coordinatesArray[orbitBodyIndex*3];
        nbodyvar& orbitBodyY = coordinatesArray[orbitBodyIndex*3+1];
        nbodyvar& orbitBodyZ = coordinatesArray[orbitBodyIndex*3+2];
        nbodyvar& orbitBodyMass = massesArray[orbitBodyIndex];

        nbodyvar distance = calculateDistance(orbitBodyX, orbitBodyY, orbitBodyZ, x, y, z);
        nbodyvar visviva = calculateVisviva(orbitBodyMass, distance, distance);

        nbodyvar theta = atan(y/x);
        nbodyvar phi   = acos(z/distance);

        if (x <= 0){
            xV = visviva * sin(theta) * sin(phi);
            yV = -visviva * cos(theta) * sin(phi);
        } else {
            xV = -visviva * sin(theta) * sin(phi);
            yV = visviva * cos(theta) * sin(phi);
        }
        
        zV = visviva * cos(phi);
        //std::cout << "<" << xV << "," << yV << "," << zV << ">\n";
    }

    void bodygen::generateBodies(){
        generateCoordinates(coordinatesArray);      // modify the coordinate array referenced by cPtr
        generateMasses(massesArray, coordinatesArray);     // modify the mass array referenced by mPtr based on the bodies positions referenced by cPtr
        generateOrbitVelocities(velocitiesArray, coordinatesArray);
        //generateVelocities(vPtr, cPtr); // modify the velocity array referenced by vPtr based on the bodies positions referenced by cPtr
    }

}