#ifndef NBODY_H
#define NBODY_H
//Type define variables for body object
typedef double nbodyvar;

//define physics values
#define PI          3.141592653589793238462643383279502884L /* pi */
#define G           1.0//6.67300E-11 /* gravitational constant */

//define simulation restrictions
#define MASS_MIN    0.0000000001//0.001
#define MASS_MAX    0.00000001

#define V_MAX       0//0.1
#define V_BOOST     1

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <iostream>


#ifdef WINDOWS_PLATFORM
    //#define PLATFORM WINDOWS
    //std::cout << "On Windows" << std::endl;
#elif  UNIX_PLATFORM
    //std::cout << "On Linuc" << std::endl;
#endif

//#define DEBUG_COMPUTE 
//#define DEBUG_OPENCL

#ifdef DEBUG_COMPUTE
    #define DEBUG_COMPUTE_MESSAGE(x) std::cout << x << std::endl;
#else
    #define DEBUG_COMPUTE_MESSAGE(x)
#endif

#ifdef DEBUG_OPENCL
    #define DEBUG_OPENCL_INFO(device, info, infoName)  std::cout << infoName << ": " << device.getInfo<info>() <<  std::endl;
    #define DEBUG_OPENCL_EVENT_DURATION_MS(event)  std::cout << "MilliSec: " << (event.getProfilingInfo<CL_PROFILING_COMMAND_END>() - event.getProfilingInfo<CL_PROFILING_COMMAND_START>()) / 1000000 <<  std::endl;
#else
    #define DEBUG_OPENCL_INFO(device, info)
    #define DEBUG_OPENCL_EVENT_DURATION_MS(event)
#endif




#endif //NBODY_H