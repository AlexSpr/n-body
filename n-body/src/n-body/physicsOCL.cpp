#include "physicsOCL.h"
#include "OpenCL/openclhelper.h"

#if defined (__APPLE__) || defined(MACOSX)
    static const std::string CL_GL_SHARING_EXT = "cl_APPLE_gl_sharing";
#else
    static const std::string CL_GL_SHARING_EXT = "cl_khr_gl_sharing";
#endif

namespace NBody {

    typedef struct {
        cl::Device d;
        cl::CommandQueue q;
        cl::Program p;
        cl::Kernel k;
        cl::Buffer i;
        cl::size_t<3> dims;
    } process_params;

    typedef struct {
        GLuint prg;
        GLuint vao;
        GLuint vbo;
        cl::BufferGL tmp;
    } render_params;

    process_params params;
    render_params rparams;


    bool checkExtnAvailability(cl::Device pDevice, std::string pName){
        bool ret_val = true;
        try {
            // find extensions required
            std::string exts = pDevice.getInfo<CL_DEVICE_EXTENSIONS>();
            std::stringstream ss(exts);
            std::string item;
            int found = -1;
            while (std::getline(ss,item,' ')) {
                if (item==pName) {
                    found=1;
                    break;
                }
            }
            if (found==1) {
                std::cout<<"Found CL_GL_SHARING extension: "<<item<<std::endl;
                ret_val = true;
            } else {
                std::cout<<"CL_GL_SHARING extension not found\n";
                ret_val = false;
            }
        } catch (cl::Error err) {
            std::cout << err.what() << "(" << err.err() << ")" << std::endl;
        }
        return ret_val;
    }

    physicsOCL::physicsOCL(u_int n, int nStatic, float dt, GLFWwindow * windowPtr){
        GLFWwindow * window = windowPtr;//(GLFWwindow*)windowPtr;
        this->n = n;
        this->nStatic = nStatic;
        this->dt = dt;
        linearTrigRange = ((n-1)*n)/2;

        std::vector<cl::Platform> all_platforms;
        cl::Platform::get(&all_platforms);

        default_platform = all_platforms[0]; //set default platform

        std::vector<cl::Device> all_devices;
        default_platform.getDevices(CL_DEVICE_TYPE_DEFAULT, &all_devices);   //CL_DEVICE_TYPE_GPU

        default_device = all_devices[0]; //set default device

        #ifdef __linux__
            cl_context_properties cps[] = {
                CL_GL_CONTEXT_KHR, (cl_context_properties)glfwGetGLXContext(window),
                CL_GLX_DISPLAY_KHR, (cl_context_properties)glfwGetX11Display(),
                CL_CONTEXT_PLATFORM, (cl_context_properties)default_platform(),
                0
            };
        #endif
        #ifdef _WIN32
                cl_context_properties cps[] = {
                    CL_GL_CONTEXT_KHR, (cl_context_properties)glfwGetWGLContext(window),
                    CL_WGL_HDC_KHR, (cl_context_properties)GetDC(glfwGetWin32Window(window)),
                    CL_CONTEXT_PLATFORM, (cl_context_properties)lPlatform(),
                    0
                };
        #endif

        if (checkExtnAvailability(default_device, CL_GL_SHARING_EXT)) {
            std::cout << "Sharing Supported" << std::endl;
        } 

        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_NAME, "CL_DEVICE_NAME");
        //std::cout << default_device.getInfo<CL_DEVICE_ADDRESS_BITS>() << std::endl;
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_ADDRESS_BITS, "CL_DEVICE_ADDRESS_BITS");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_DOUBLE_FP_CONFIG, "CL_DEVICE_DOUBLE_FP_CONFIG");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_GLOBAL_MEM_SIZE, "CL_DEVICE_GLOBAL_MEM_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_LOCAL_MEM_SIZE, "CL_DEVICE_LOCAL_MEM_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_COMPUTE_UNITS, "CL_DEVICE_MAX_COMPUTE_UNITS");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_CONSTANT_ARGS, "CL_DEVICE_MAX_CONSTANT_ARGS");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, "CL_DEVICE_MAX_MEM_ALLOC_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_PARAMETER_SIZE, "CL_DEVICE_MAX_PARAMETER_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_WORK_GROUP_SIZE, "CL_DEVICE_MAX_WORK_GROUP_SIZE");
        DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");
        //DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_WORK_ITEM_SIZES, "CL_DEVICE_MAX_WORK_ITEM_SIZES");
        //DEBUG_OPENCL_INFO(default_device, CL_DEVICE_MAX_WORK_ITEM_SIZES, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");

        context = cl::Context(default_device, cps);

        queue = cl::CommandQueue(context, default_device, CL_QUEUE_PROFILING_ENABLE);

        prof_event = cl::Event();
    }

    physicsOCL::physicsOCL(u_int n, int nStatic, float dt){
        this->n = n;
        this->nStatic = nStatic;
        this->dt = dt;
        linearTrigRange = ((n-1)*n)/2;

        std::vector<cl::Platform> all_platforms;
        cl::Platform::get(&all_platforms);

        default_platform = all_platforms[0]; //set default platform

        std::vector<cl::Device> all_devices;
        default_platform.getDevices(CL_DEVICE_TYPE_DEFAULT, &all_devices);   //CL_DEVICE_TYPE_GPU

        default_device = all_devices[0]; //set default device

        context = cl::Context(default_device);

        queue = cl::CommandQueue(context, default_device, CL_QUEUE_PROFILING_ENABLE);

        prof_event = cl::Event();
    };

    physicsOCL::~physicsOCL(){

    };

    cl::Kernel physicsOCL::compileKernel(const std::string& kernelName){
        std::ifstream kernelFile("OpenCL/kernels/" + kernelName + ".cl"); //opencl/
        std::string src(std::istreambuf_iterator<char>(kernelFile), (std::istreambuf_iterator<char>()));

        cl::Program::Sources sources(1, std::make_pair(src.c_str(), src.length() + 1));
        cl::Program program(context, sources);

        program.build("-cl-std=CL1.2");

        cl::Kernel kernel(program, kernelName.c_str() );
        return kernel;
    }



    void physicsOCL::createCoordinateBuffer(){
        coordinateBuffer = cl::Buffer(context, CL_MEM_READ_WRITE |  CL_MEM_COPY_HOST_PTR, sizeof(nbodyvar) * n * 3, Coordinates); //CL_MEM_HOST_READ_ONLY |
    }

    void physicsOCL::createVelocityBuffer(){
        velocityBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(nbodyvar) * n * 3, velocitiesArray);
    }

    void physicsOCL::createGravityVectorBuffer(){
        gravityVectorBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(nbodyvar) * n * n * 3, gravityVectors);
    }

    void physicsOCL::createMassBuffer(){
        massBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(nbodyvar) * n, massesArray);
    }

    void physicsOCL::setCoordinateBufferFromOpenGL(unsigned int VBO){
        cl_int error;
        CLGLcoordinateBuffer = cl::BufferGL( context, CL_MEM_READ_WRITE, VBO, &error);
        if (error !=CL_SUCCESS) {
            std::cout << getErrorString(error)  << std::endl;
        }
    }

    void physicsOCL::setUpCLGLPositionKernel(){
        cl_int result;
        positionKernel = compileKernel("positionKernel");
        positionKernel.setArg(0, CLGLcoordinateBuffer);
        positionKernel.setArg(1, velocityBuffer);
        positionKernel.setArg(2, dt);
    }

    void physicsOCL::setUpCLGLGravityKernel(){
        cl_int result;
        gravityKernel = compileKernel("gravityKernel");
        gravityKernel.setArg(0, CLGLcoordinateBuffer);
        gravityKernel.setArg(1, massBuffer);
        gravityKernel.setArg(2, gravityVectorBuffer);
        gravityKernel.setArg(3, n);
        gravityKernel.setArg(4, G);
    }

    void physicsOCL::setUpPostionKernel(){
        positionKernel = compileKernel("positionKernel");
        positionKernel.setArg(0, coordinateBuffer);
        positionKernel.setArg(1, velocityBuffer);
        positionKernel.setArg(2, dt);
    }

    void physicsOCL::setUpVerletVelocityKernel(){
        verletVelocityKernel = compileKernel("verletVelocityKernel");
        verletVelocityKernel.setArg(0, velocityBuffer);
        verletVelocityKernel.setArg(1, massBuffer);
        verletVelocityKernel.setArg(2, gravityVectorBuffer);
        verletVelocityKernel.setArg(3, n);
        verletVelocityKernel.setArg(4, dt);
        //verletVelocityKernel.setArg(5, NULL);
    }

    void physicsOCL::setUpGravityKernel(){
        gravityKernel = compileKernel("gravityKernel");
        gravityKernel.setArg(0, coordinateBuffer);
        gravityKernel.setArg(1, massBuffer);
        gravityKernel.setArg(2, gravityVectorBuffer);
        gravityKernel.setArg(3, n);
        gravityKernel.setArg(4, G);
    }


    void physicsOCL::acquireOpenGLObjects(){
        cl_int result;
        objs.clear();
        objs.push_back(CLGLcoordinateBuffer);
        result = queue.enqueueAcquireGLObjects(&objs,NULL,&prof_event);
        result |= queue.finish();
        //prof_event.wait();
        if (result !=CL_SUCCESS) {
            std::cout<<"Failed acquiring GL object: "<<result<<std::endl;
            exit(248);
        }
    }

    void physicsOCL::releaseOpenGLObjects(){
        cl_int result;
        result = queue.enqueueReleaseGLObjects(&objs);
        //result |= clFinish(queue); 
        result |= queue.finish();
        //prof_event.wait();
        if (result !=CL_SUCCESS) {
            std::cout<<"Failed releasing GL object: "<<result<<std::endl;
            exit(247);
        }
    }

    void physicsOCL::computePositions(){
        cl_int result = queue.enqueueNDRangeKernel(positionKernel, (nStatic), cl::NDRange(n), cl::NullRange, NULL, &prof_event); 
        result |= queue.finish();
        if (result !=CL_SUCCESS) {
            std::cout << getErrorString(result)  << std::endl;
        }  
    }

    void physicsOCL::computeVerletVelocity(){
        cl_int result = queue.enqueueNDRangeKernel(verletVelocityKernel, cl::NullRange, cl::NDRange(n), cl::NullRange, NULL, &prof_event); 
        result |= queue.finish();
        if (result != CL_SUCCESS) {
            std::cout << getErrorString(result)  << std::endl;
        } 
    }

    void physicsOCL::computeGravity(){
        //Range from 0 minus trigrange - 1
        cl_int result = queue.enqueueNDRangeKernel(gravityKernel, cl::NullRange, cl::NDRange(linearTrigRange), cl::NullRange, NULL, &prof_event); 
        
        //prof_event.wait();

        result |= queue.finish();
        if (result != CL_SUCCESS) {
            std::cout << getErrorString(result)  << std::endl;
        } 

        
    }



    void physicsOCL::readGravVectorBuffer(){
        queue.enqueueReadBuffer(gravityVectorBuffer, CL_TRUE, 0, sizeof(nbodyvar) * n * n * 3, gravityVectors, NULL, &prof_event);
    }

    void physicsOCL::readVerletVelocityBuffer(){
        queue.enqueueReadBuffer(velocityBuffer, CL_TRUE, 0, sizeof(nbodyvar) * n * 3, velocitiesArray, NULL, &prof_event);
    }

    void physicsOCL::readPositionsBuffer(){
        queue.enqueueReadBuffer(CLGLcoordinateBuffer, CL_TRUE, 0, sizeof(nbodyvar) * n * 3, Coordinates, NULL, &prof_event);
    }



    void physicsOCL::writeToCoordinatebuffer(){
        cl_int result = queue.enqueueWriteBuffer(coordinateBuffer, CL_TRUE, 0, sizeof(nbodyvar) * n * 3, Coordinates);
        result |= queue.finish();
    }

}


















