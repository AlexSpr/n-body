#ifndef PHYSICSOCL_H
#define PHYSICSOCL_H

#define __CL_ENABLE_EXCEPTIONS

#ifdef _WIN32
    #define GLFW_EXPOSE_NATIVE_WIN32
    #define GLFW_EXPOSE_NATIVE_WGL
#endif

#ifdef __linux__
    #define GLFW_EXPOSE_NATIVE_X11
    #define GLFW_EXPOSE_NATIVE_GLX
#endif

#include "Core.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include "info.h"
#include <iostream>
#include <math.h>
#include <random>
#include <fstream>
#include <sstream>
#include <CL/cl.hpp>
#include <CL/cl_gl.h>

namespace NBody {

    class physicsOCL{
        public:
            physicsOCL(u_int n, int nStatic, float dt);
            physicsOCL(u_int n, int nStatic, float dt, GLFWwindow * windowPtr);
            ~physicsOCL();

            void setSimulationData(nbodyvar* cPtr, nbodyvar* gPtr, nbodyvar* vPtr, nbodyvar* mPtr){
                this->Coordinates = cPtr;
                this->velocitiesArray = vPtr;
                this->gravityVectors = gPtr;
                this->massesArray = mPtr;
            }

            cl::Kernel compileKernel(const std::string& kernelFileName);

            void acquireOpenGLObjects();
            void releaseOpenGLObjects();

            void createCoordinateBuffer();
            void createVelocityBuffer();
            void createMassBuffer();
            void createGravityVectorBuffer();

            void setCoordinateBufferFromOpenGL(unsigned int VBO);

            void setUpPostionKernel();
            void setUpCLGLPositionKernel();
            void setUpVerletVelocityKernel();
            void setUpGravityKernel();
            void setUpCLGLGravityKernel();

            void computePositions();
            void computeVerletVelocity();
            void computeGravity();

            void readPositionsBuffer();
            void readVerletVelocityBuffer();
            void readGravVectorBuffer();
            
            

            void writeToCoordinatebuffer();

        private:
            int n;
            int nStatic;
            int linearTrigRange;
            float dt;
            std::vector<cl::Memory> objs;
            nbodyvar* Coordinates;
            nbodyvar* gravityVectors;
            nbodyvar* massesArray;
            nbodyvar* velocitiesArray;


            cl::Platform default_platform;
            cl::Device default_device;
            cl::Context context;
            cl::CommandQueue queue;

            cl::Event prof_event;

            cl::BufferGL CLGLcoordinateBuffer;
            cl::Buffer coordinateBuffer;

            cl::Buffer gravityVectorBuffer;

            cl::Buffer massBuffer;

            cl::Buffer velocityBuffer;

            cl::Kernel gravityKernel;
            cl::Kernel verletVelocityKernel;
            cl::Kernel positionKernel;

            //cl::BufferGL cgl_context;

    };

}

#endif //PHYSICSOCL_H