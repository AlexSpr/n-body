#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void verletVelocityKernel(__global double* velocityArray, __global double* massArray, __global double* gravitationalForceVector, const unsigned int n, const float dt){
    
    size_t x = get_global_id(0) * 3;
    size_t y = get_global_id(0) * 3 + 1;
    size_t z = get_global_id(0) * 3 + 2;

    size_t gravityIndex = get_global_id(0) * 3 * n;

    double bodyMass = massArray[get_global_id(0)];
    
    for (int b = 0; b < n; b++){  
        velocityArray[x] += gravitationalForceVector[gravityIndex + b*3]   / bodyMass * dt / 2 ;
        velocityArray[y] += gravitationalForceVector[gravityIndex + b*3+1] / bodyMass * dt / 2 ;
        velocityArray[z] += gravitationalForceVector[gravityIndex + b*3+2] / bodyMass * dt / 2 ;
    }
    
}