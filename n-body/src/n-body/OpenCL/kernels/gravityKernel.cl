#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#define minDist 0.000000000000000000000000000000000001

__kernel void gravityKernel(__global double* CoordinatesArray,__global double* MassesArray, __global double* gravitationalForceVector, const unsigned int n, const double G){

    size_t iBody = (size_t)((-1+sqrt(8*(double)get_global_id(0)+1)) / 2);
    size_t jBody = (get_global_id(0) - iBody * (iBody+1) * 0.5);
    
    iBody += 1;

    size_t lowerTrigIndex = (jBody * n * 3) + iBody * 3;
    size_t upperTrigIndex = (iBody * n * 3) + jBody * 3;

    double gravitationalForceX = CoordinatesArray[iBody * 3]   - CoordinatesArray[jBody * 3];
    double gravitationalForceY = CoordinatesArray[iBody * 3+1] - CoordinatesArray[jBody * 3+1];
    double gravitationalForceZ = CoordinatesArray[iBody * 3+2] - CoordinatesArray[jBody * 3+2];

    double gravForce = G * MassesArray[iBody] * MassesArray[jBody] / pow(sqrt( pow(gravitationalForceX,2) + pow(gravitationalForceY,2) + pow(gravitationalForceZ,2) + minDist), 3);

    gravitationalForceX   *=  gravForce ;
    gravitationalForceY   *=  gravForce;
    gravitationalForceZ   *=  gravForce;

    gravitationalForceVector[lowerTrigIndex]   =  gravitationalForceX;
    gravitationalForceVector[lowerTrigIndex+1] =  gravitationalForceY;
    gravitationalForceVector[lowerTrigIndex+2] =  gravitationalForceZ;

    gravitationalForceVector[upperTrigIndex]   =  -gravitationalForceX;
    gravitationalForceVector[upperTrigIndex+1] =  -gravitationalForceY;
    gravitationalForceVector[upperTrigIndex+2] =  -gravitationalForceZ;

}