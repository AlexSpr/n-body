#pragma OPENCL EXTENSION cl_khr_fp64 : enable

__kernel void positionKernel(__global double* positionsArray, __global double* velocitiesArray, const float dt){
    
    size_t x = get_global_id(0) * 3;
    size_t y = get_global_id(0) * 3 + 1;
    size_t z = get_global_id(0) * 3 + 2;

    positionsArray[x] = positionsArray[x] + velocitiesArray[x] * dt;
    positionsArray[y] = positionsArray[y] + velocitiesArray[y] * dt;
    positionsArray[z] = positionsArray[z] + velocitiesArray[z] * dt;
    
}