#ifndef OPENCLHELPER_H
#define OPENCLHELPER_H

#include <CL/cl.hpp>
#include <fstream>

cl::Program createProgram(const std::string& file);

const char *getErrorString(cl_int error);

#endif //OPENCLHELPER_H

