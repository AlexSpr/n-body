
#include "Camera.h"

#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
//#include <GLFW/glfw3.h>

namespace NBody {
    
    void Camera::ProcessInput(GLFWwindow * window){
         // adjust accordingly
        
        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS){

            m_Position -= m_CameraDirection * m_CameraSpeed;
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS){
            m_Position += m_CameraDirection * m_CameraSpeed;
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS){

            pitch(1.0f);
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS){
            pitch(-1.0f);
        }

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS){
            yaw(1.0f);
        }
        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS){
            yaw(-1.0f);
        }

        if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
            roll(-1.0f);
        }

        if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS){
            roll(1.0f);
        }
        RecalculateViewMatrix();
    }
    
    /*
    Camera::Camera(float left, float right, float bottom, float top) 
        : m_ProjectionMatrix(glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 5000.0f)),
        m_ViewMatrix(1.0f) {
            m_ViewProjectionMatrix  = m_ProjectionMatrix * m_ViewMatrix;
        }
    */
    Camera::Camera(float fov, float aspect, float prespeectiveNearPlane, float prespeectiveFarPlane) :
        m_FoV(fov) {
        m_ViewMatrix = glm::mat4(1.0f);
        m_ModelMatrix = glm::mat4(1.0f);
        //Up = glm::vec3(0.0f, 1.0f, 0.0f); 
        m_cameraUp = glm::vec3(0.0f, 1.0f, 0.0f); 


        m_ProjectionMatrix = glm::perspective(glm::radians(m_FoV), aspect, prespeectiveNearPlane, prespeectiveFarPlane);
        
        m_ViewMatrix = glm::lookAt(glm::vec3(0.0, 0.0, 0.0), m_CameraTarget, glm::vec3(0.0, 1.0, 0.0));
        //m_ViewMatrix = glm::translate(m_ViewMatrix, glm::vec3(0.0f, 0.0f, -2600.0f));
        //m_ModelMatrix =  glm::rotate(m_ModelMatrix, glm::radians(-35.0f), glm::vec3(1.0f, 0.0f, 0.0f));

        m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
    }
    
    void Camera::pitch(float angle){
        glm::quat rotation_quaternion =  glm::angleAxis(glm::radians(angle), m_CameraRight);

        m_Position = rotation_quaternion * m_Position;
        Up = rotation_quaternion * Up;
    };

    void Camera::yaw(float angle){
        glm::quat rotation_quaternion =  glm::angleAxis(glm::radians(angle), m_cameraUp);
        m_Position = rotation_quaternion * m_Position;
        Up = rotation_quaternion * Up;
    };


    void Camera::roll(float angle){
        glm::quat rotation_quaternion =  glm::angleAxis(glm::radians(angle), m_CameraDirection);
        Up = rotation_quaternion * Up;
    };
    
    void Camera::RecalculateViewMatrix(){
        m_CameraDirection = glm::normalize(m_Position - m_CameraTarget);
        m_CameraRight = glm::normalize(glm::cross(m_CameraDirection, Up));
        m_cameraUp = glm::cross(m_CameraDirection, m_CameraRight);
        //std::cout << "UP"<<" X: " << Up.x << "\tY: " << Up.y << "\tZ: "<< Up.z << std::endl;
        //std::cout << "m_Position"<<" X: " << m_Position.x << "\tY: " << m_Position.y << "\tZ: "<< m_Position.z << std::endl;
        //std::cout << "Camera Up"<<" X: " << m_cameraUp.x << "\tY: " << m_cameraUp.y << "\tZ: "<< m_cameraUp.z << std::endl;
        m_ViewMatrix = glm::lookAt(m_Position, m_CameraTarget, m_cameraUp);
        m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
    }

    void Camera::RecalculatePerspective(){
        //m_ProjectionMatrix = glm::perspective(glm::radians(m_FoV), aspect, prespeectiveNearPlane, prespeectiveFarPlane);
    }
        /*
        glm::mat4 transform = glm::translate(glm::mat4(1.0f), m_Position)
            * glm::rotate(glm::mat4(1.0f), glm::radians(m_Rotation), glm::vec3(m_xPitch, m_yYaw, m_zRoll));
        
        m_ViewMatrix = glm::inverse(transform);
        m_ViewProjectionMatrix = m_ProjectionMatrix * m_ViewMatrix;
        */
    

}