#ifndef CAMERA_H
#define CAMERA_H

//#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace NBody {

    class Camera {
        public:
            //Camera(float left, float right, float bottom, float top);
            Camera(float fov, float aspect, float prespeectiveNearPlane = 0.01f, float prespeectiveFarPlane = 10000.0f);
            ~Camera();

            void SetPosition(const glm::vec3& position) { m_Position = position; RecalculateViewMatrix(); }
            void SetXRotation(float rotation) {m_Rotation = rotation; RecalculateViewMatrix(); }
            void SetYRotation(float rotation) {m_Rotation = rotation; RecalculateViewMatrix(); }
            void SetZRotation(float rotation) {m_Rotation = rotation; RecalculateViewMatrix(); }

            void pitch(float angle);
            void yaw(float angle);
            void roll(float angle);

            const glm::vec3& GetPosition() const { return m_Position; }
            float GetRotation() const { return m_Rotation; }

            const glm::mat4& GetProjectionMatrix() const { return m_ProjectionMatrix; }
		    const glm::mat4& getViewMatrix() const { return m_ViewMatrix; }
		    const glm::mat4& GetViewProjectionMatrix() const { return m_ViewProjectionMatrix; }

            void ProcessInput(GLFWwindow * window);
        private:
            void RecalculateViewMatrix();
            void RecalculatePerspective();

        public:
            glm::vec3 m_Position = {0.0f, 0.0f, 0.0f};
            glm::vec3 m_CameraTarget = {0.0f, 0.0f, 0.0f};
            glm::vec3 m_Front = glm::vec3(0.0f, 0.0f, -1.0f);
            glm::vec3 m_CameraDirection = glm::normalize(m_Position - m_CameraTarget);
            
            //glm::vec3 Up;
            glm::vec3 m_cameraUp;
            glm::vec3 m_CameraRight;


            glm::vec3 Up = glm::vec3(0.0f, 1.0f, 0.0f);
            float m_FoV = 0.0f;
            float m_CameraSpeed = 10.0f;

            float m_Rotation = 0.0f;

            float m_xPitch = 1.0f;
            float m_yYaw = 1.0f;
            float m_zRoll = 0.0f;

            glm::mat4 m_ProjectionMatrix;
            glm::mat4 m_ViewMatrix;
            glm::mat4 m_ViewProjectionMatrix;
            glm::mat4 m_ModelMatrix;
            
            

    };
}

#endif //CAMERA_H