#ifndef BUFFER_H
#define BUFFER_H

#include <cstdint>
#include <string>
#include <vector>

namespace NBody {

    enum class ShaderDataType {
        none = 0, Double, Double2, Double3, Double4, Float, Float2, Float3, Float4, Mat3, Mat4, Int, Int2, Int3, Int4, Boolean
    };

    static uint32_t ShaderDataTypeSize(ShaderDataType type) {
        switch (type) {
            case ShaderDataType::Double:	return 8;
            case ShaderDataType::Double2:	return 8 * 2;
            case ShaderDataType::Double3:	return 8 * 3;
            case ShaderDataType::Double4:	return 8 * 4;
            case ShaderDataType::Float:		return 4;
            case ShaderDataType::Float2:	return 4 * 2;
            case ShaderDataType::Float3:	return 4 * 3;
            case ShaderDataType::Float4:	return 4 * 4;
            case ShaderDataType::Mat3:		return 4 * 3 * 3;
            case ShaderDataType::Mat4:		return 4 * 4 * 4;
            case ShaderDataType::Int:		return 4;
            case ShaderDataType::Int2:		return 4 * 2;
            case ShaderDataType::Int3:		return 4 * 3;
            case ShaderDataType::Int4:		return 4 * 4;
            case ShaderDataType::Boolean:		return 1;
        }

        return 0;
    }

    enum class PixelFormat {
        none = 0, Red, Green, Blue, RGB, BGR, RGBA, BGRA
    };

    static uint32_t PixelFormatSize(PixelFormat type) {
        switch (type) {
            case PixelFormat::Red:	    return 1;
            case PixelFormat::Green:	return 1;
            case PixelFormat::Blue:	    return 1;
            case PixelFormat::RGB:	    return 3;
            case PixelFormat::BGR:		return 3;
            case PixelFormat::RGBA:	    return 4;
            case PixelFormat::BGRA:	    return 4;
        }

        return 0;
    }

    struct BufferElement
    {
        std::string Name;
        ShaderDataType Type;
        uint32_t Size;
        uint32_t Offset;
        bool Normalized;

        BufferElement() {}

        BufferElement(ShaderDataType type, const std::string& name, bool normalized = false) : Name(name), Type(type), Size(ShaderDataTypeSize(type)), Offset(0), Normalized(normalized) {

        }

        uint32_t GetComponentCount() const {
            switch (Type) {
                case ShaderDataType::Double:	return 1;
                case ShaderDataType::Double2:	return 2;
                case ShaderDataType::Double3:	return 3;
                case ShaderDataType::Double4:	return 4;
                case ShaderDataType::Float:		return 1;
                case ShaderDataType::Float2:	return 2;
                case ShaderDataType::Float3:	return 3;
                case ShaderDataType::Float4:	return 4;
                case ShaderDataType::Mat3:		return 3 * 3;
                case ShaderDataType::Mat4:		return 4 * 4;
                case ShaderDataType::Int:		return 1;
                case ShaderDataType::Int2:		return 2;
                case ShaderDataType::Int3:		return 3;
                case ShaderDataType::Int4:		return 4;
                case ShaderDataType::Boolean:	return 1;
            }
            return 0;
        }
    };

    class BufferLayout {
    public:
        BufferLayout() {}

        BufferLayout(const std::initializer_list<BufferElement>& elements): m_Elements(elements){
            CalculateOffset();
        }

        inline uint32_t GetStride() const { return m_Stride; }
        inline const std::vector<BufferElement>& GetElements() const { return m_Elements;  }

        std::vector<BufferElement>::iterator begin() { return m_Elements.begin(); }
        std::vector<BufferElement>::iterator end() { return m_Elements.end(); }

        std::vector<BufferElement>::const_iterator begin() const { return m_Elements.begin(); }
        std::vector<BufferElement>::const_iterator end() const { return m_Elements.end(); }

    private:
        void CalculateOffset() {
            uint32_t offset = 0;
            m_Stride = 0;
            for (auto& element : m_Elements) {
                element.Offset = offset;
                offset += element.Size;
                m_Stride += element.Size;
            }
        }

    private:
        std::vector<BufferElement> m_Elements;
        uint32_t m_Stride = 0;
    };

    class VertexBuffer {
    public:
        virtual ~VertexBuffer(){}

        virtual void Bind() const = 0;
        virtual void Unbind() const = 0;

        virtual void SetLayout(const BufferLayout& layout) = 0;
        virtual const BufferLayout& GetLayout() const = 0;
        virtual const uint32_t GetID() const = 0;

        static VertexBuffer* Create(float* vertices, uint32_t size);
    };

    class IndexBuffer {
    public:
        virtual ~IndexBuffer() {}

        virtual void Bind() const = 0;
        virtual void Unbind() const = 0;

        virtual uint32_t GetCount() const = 0;

        static IndexBuffer* Create(uint32_t* indices, uint32_t count);

    };

    class PixelBuffer {
    public:
        virtual ~PixelBuffer(){}

        virtual void Bind() const = 0;
        virtual void Unbind() const = 0;
        virtual void ReadPixels() const = 0;
        virtual void GetPixels(unsigned char* pixels) const = 0;
        //virtual void SetLayout(const BufferLayout& layout) = 0;
        //virtual const BufferLayout& GetLayout() const = 0;
        virtual const uint32_t GetID() const = 0;

        static VertexBuffer* Create(float* vertices, uint32_t size);
    };

}

#endif //BUFFER_H