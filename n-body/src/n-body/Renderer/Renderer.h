#ifndef RENDERER_H
#define RENDERER_H

#include <glm/glm.hpp>

namespace NBody {

    class Renderer {
        public:
            virtual void SetCamera(Camera* camera) = 0;
            
            virtual void SetClearColor(const glm::vec4& color) = 0;
            virtual void Clear() = 0;
            //static  void 
    };

}

#endif //RENDERER_H
