#ifndef WINDOW_H
#define WINDOW_H

#include "Core.h"
//#include "Platform/Linux/LinuxWindow.h"

namespace NBody {

	struct WindowProps {
		std::string Title;
		unsigned int Width;
		unsigned int Height;

		WindowProps(const std::string& title = "N-Body Simulation",
						unsigned int width = 1920,
						unsigned int height = 1080)
				: Title(title), Width(width), Height(height) {}

	};

	class Window {
		public:
			//Window() {}
			virtual ~Window() {}

			virtual void OnUpdate() = 0;

			virtual bool ShouldClose() = 0;

			virtual unsigned int GetWidth() const = 0;
			virtual unsigned int GetHeight() const = 0;
			virtual float GetAspect() const = 0;

			virtual void*  GetWindow() const = 0; 

			static Window* Create(const WindowProps& props = WindowProps());
	};

}

#endif //WINDOW_H