#include "info.h"
#include <iostream>
#include <math.h>

namespace NBody {

    void info::printArray(double* array, int& nbody, char const* statment, int offset){
        std::cout<< "\n" << statment << "\n";
        for (int i = offset; i < nbody + offset; i++){
                std::cout << array[i] << "\t";   
        }
        std::cout << "\n";
    }

    void info::printVectorArray(double* array, int& nbody, char const* statment, int offset){
        std::cout<< "\n" << statment << "\n";
        for (int i = offset ; i < nbody + offset; i++){
            std::cout << "<"<< array[i*3] << ","<< array[i*3+1] <<","<< array[i*3+2] <<">\n"; 
        }
        std::cout << "\n";
    }

    void info::printMatrix(double* array, int& nbody, char const* statment){
        std::cout<< "\n" << statment << "\n";
        for (int i = 0; i < nbody; i++) {
            for (int j = 0; j < nbody; j++){
                std::cout << (array+i*nbody)[j] << "\t";   
            }
            std::cout << "\n";
        }
    }

    void info::printVectorMatrix(double* array, int& nbody, char const* statment){
        std::cout<< "\n" << statment << "\n";
        for (int i = 0; i < nbody; i++) {
            for (int j = 0; j < nbody; j++){
                std::cout << "<"<< (array+i*nbody*3)[j*3] << ","<< (array+i*nbody*3)[j*3+1] <<","<< (array+i*nbody*3)[j*3+2] <<">\t";   
            }
            std::cout << "\n";
        }
    }

    void info::findNANinVectorArray(double* array, int& nbody, char const* statment){
        int nanCounter = 0;
        int firstNanIndex = 0;
        for (int i = 0; i < nbody ; i++) {
            if (isnan(array[i * 3 ])){
                nanCounter += 1;
                if (nanCounter == 1) {
                    firstNanIndex = i;
                }
            }
            
        }
        if(nanCounter > 0) {
            std::cout<< "\n" << statment << "\n";
            std::cout<< "Found: " << nanCounter << " nans \n";
            std::cout<< "First NaN at: " << firstNanIndex << "\n";
        }
    }

    void info::findNANinVectorMatrix(double* array, int& nbody, char const* statment){
        int nanCounter = 0;
        int firstNanIndex = 0;
        for (int i = 0; i < nbody; i++) {
            for (int j = 0; j < nbody; j++){
                if (isnan((array+i*nbody*3)[j * 3] )){
                    nanCounter += 1;
                    if (nanCounter == 1) {
                        firstNanIndex = i;
                    }
                }
            }
        }
        if(nanCounter > 0) {
            std::cout<< "\n" << statment << "\n";
            std::cout<< "Found: " << nanCounter << " nans \n";
            std::cout<< "First NaN at: " << firstNanIndex << "\n";
        }
    }

    void info::findVectorsInArrayConditional(double* array, int& nbody, double conditionVector[3], char const* arrayName){
        std::cout<< "\n" << "Looking for Vectors <" << conditionVector[0] << "," << conditionVector[1] << "," << conditionVector[2] << "> in array " << arrayName <<  std::endl;
        bool sequence = true;
        unsigned int count = 0;

        for (int i = 0; i < nbody; i++){
            if (array[i*3] == conditionVector[0] && array[i*3+1] == conditionVector[1] && array[i*3+2] == conditionVector[2]){
                std::cout << "Index: " << i << std::endl;
                sequence = true;
                count += 1;
            }
            else {
                if (sequence)
                    std::cout << std::endl;
                sequence = false;
            }

                
            
                //std::cout << "<"<< array[i*3] << ","<< array[i*3+1] <<","<< array[i*3+2] <<">\n"; 
        }
        std::cout << "Found: " << count <<"\n";
    }

    void info::findVectorsInMatrixColumnConditional(double* matrix, int& nbody, int column, double conditionVector[3], char const* arrayName){
        std::cout<< "\n" << "Looking for Vectors <" << conditionVector[0] << "," << conditionVector[1] << "," << conditionVector[2] << "> in array " << arrayName <<  std::endl;
        bool sequence = true;
        unsigned int count = 0;
        for (int i = 0; i < nbody; i ++) {

            if (matrix[3 * i * 3 + column * 3] == conditionVector[0] && matrix[3 * i * 3 + column * 3 + 1] == conditionVector[1] && matrix[3 * i * 3 + column * 3 + 2] == conditionVector[2]){
                std::cout << "Row Index: " << i << std::endl;
                sequence = true;
                count += 1;
            }
            else {
                if (sequence)
                    std::cout << std::endl;
                sequence = false;
            }
        }
    }
}

/*
void info::printOpenCLError(cl_int error){
    std::cout << getErrorString( error ) << std::endl;
} 

const char[] info::getErrorString(cl_int error)
{
switch(error){
    // run-time and JIT compiler errors
    case 0: return "CL_SUCCESS";
    case -1: return "CL_DEVICE_NOT_FOUND";
    case -2: return "CL_DEVICE_NOT_AVAILABLE";
    case -3: return "CL_COMPILER_NOT_AVAILABLE";
    case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
    case -5: return "CL_OUT_OF_RESOURCES";
    case -6: return "CL_OUT_OF_HOST_MEMORY";
    case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
    case -8: return "CL_MEM_COPY_OVERLAP";
    case -9: return "CL_IMAGE_FORMAT_MISMATCH";
    case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
    case -11: return "CL_BUILD_PROGRAM_FAILURE";
    case -12: return "CL_MAP_FAILURE";
    case -13: return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
    case -14: return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
    case -15: return "CL_COMPILE_PROGRAM_FAILURE";
    case -16: return "CL_LINKER_NOT_AVAILABLE";
    case -17: return "CL_LINK_PROGRAM_FAILURE";
    case -18: return "CL_DEVICE_PARTITION_FAILED";
    case -19: return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";

    // compile-time errors
    case -30: return "CL_INVALID_VALUE";
    case -31: return "CL_INVALID_DEVICE_TYPE";
    case -32: return "CL_INVALID_PLATFORM";
    case -33: return "CL_INVALID_DEVICE";
    case -34: return "CL_INVALID_CONTEXT";
    case -35: return "CL_INVALID_QUEUE_PROPERTIES";
    case -36: return "CL_INVALID_COMMAND_QUEUE";
    case -37: return "CL_INVALID_HOST_PTR";
    case -38: return "CL_INVALID_MEM_OBJECT";
    case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
    case -40: return "CL_INVALID_IMAGE_SIZE";
    case -41: return "CL_INVALID_SAMPLER";
    case -42: return "CL_INVALID_BINARY";
    case -43: return "CL_INVALID_BUILD_OPTIONS";
    case -44: return "CL_INVALID_PROGRAM";
    case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
    case -46: return "CL_INVALID_KERNEL_NAME";
    case -47: return "CL_INVALID_KERNEL_DEFINITION";
    case -48: return "CL_INVALID_KERNEL";
    case -49: return "CL_INVALID_ARG_INDEX";
    case -50: return "CL_INVALID_ARG_VALUE";
    case -51: return "CL_INVALID_ARG_SIZE";
    case -52: return "CL_INVALID_KERNEL_ARGS";
    case -53: return "CL_INVALID_WORK_DIMENSION";
    case -54: return "CL_INVALID_WORK_GROUP_SIZE";
    case -55: return "CL_INVALID_WORK_ITEM_SIZE";
    case -56: return "CL_INVALID_GLOBAL_OFFSET";
    case -57: return "CL_INVALID_EVENT_WAIT_LIST";
    case -58: return "CL_INVALID_EVENT";
    case -59: return "CL_INVALID_OPERATION";
    case -60: return "CL_INVALID_GL_OBJECT";
    case -61: return "CL_INVALID_BUFFER_SIZE";
    case -62: return "CL_INVALID_MIP_LEVEL";
    case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
    case -64: return "CL_INVALID_PROPERTY";
    case -65: return "CL_INVALID_IMAGE_DESCRIPTOR";
    case -66: return "CL_INVALID_COMPILER_OPTIONS";
    case -67: return "CL_INVALID_LINKER_OPTIONS";
    case -68: return "CL_INVALID_DEVICE_PARTITION_COUNT";

    // extension errors
    case -1000: return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
    case -1001: return "CL_PLATFORM_NOT_FOUND_KHR";
    case -1002: return "CL_INVALID_D3D10_DEVICE_KHR";
    case -1003: return "CL_INVALID_D3D10_RESOURCE_KHR";
    case -1004: return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
    case -1005: return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
    default: return "Unknown OpenCL error";
    }
}
*/