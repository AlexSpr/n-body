#ifndef LINUXWINDOW_H
#define LINUXWINDOW_H

//#include "Core.h"
#include "n-body/Window.h"
#include "n-body/Renderer/GraphicsContext.h"
#include <iostream>
#include <string>
#include <GLFW/glfw3.h>
/*
void framebuffer_size_callback(GLFWwindow* window, int width, int height){
    glViewport(width/10, height/10, width/1.2, height/1.2);
};
*/
namespace NBody {

    class LinuxWindow : public Window {

        public: 
            LinuxWindow(const WindowProps& props);
            virtual ~LinuxWindow();

            virtual void OnUpdate() override;

            virtual bool ShouldClose() override { glfwWindowShouldClose(m_Window); }

            virtual unsigned int GetWidth() const override {return m_Data.Width;}
			virtual unsigned int GetHeight() const override  {return m_Data.Height;}
            virtual float GetAspect() const override { return m_Data.Aspect; }
            
            inline virtual void* GetWindow() const { return m_Window;}
            //GLFWwindow* getWindow(){return window;}
        
        private:
            virtual void Init(const WindowProps& props);
            virtual void Shutdown();

            GLFWwindow* m_Window;
            GraphicsContext* m_Context;

            struct WindowData {
                std::string Title;
                unsigned int Width, Height;
                float Aspect;
                bool VSync;
		    };

		    WindowData m_Data;

            
    }; 

} 

#endif //LINUXWINDOW_H