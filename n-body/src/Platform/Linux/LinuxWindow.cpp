
#include "LinuxWindow.h"
#include "Platform/OpenGL/OpenGLContext.h"
#include <string>

namespace NBody {

    Window* Window::Create(const WindowProps& props) {
		return new LinuxWindow(props);
	}


    LinuxWindow::LinuxWindow(const WindowProps& props){
        Init(props);
    }

    LinuxWindow::~LinuxWindow(){
        Shutdown();
    }

    void  LinuxWindow::Init(const WindowProps& props){
        m_Data.Title = props.Title;
		m_Data.Width = props.Width;
		m_Data.Height = props.Height;
        m_Data.Aspect = (float)props.Width/(float)props.Height;


        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        m_Window = glfwCreateWindow((int)props.Width, (int)props.Height, props.Title.c_str(), NULL, NULL);

        if (m_Window == NULL)
        {
            std::cout << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
        }

        m_Context = new OpenGLContext(m_Window);
        m_Context->Init();
    }

    void LinuxWindow::Shutdown(){
        glfwDestroyWindow(m_Window);
    }

    void LinuxWindow::OnUpdate() {
		glfwPollEvents();
		m_Context->SwapBuffers();

	}


}