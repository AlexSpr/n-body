#ifndef OPENGLCONTEXT_H
#define OPENGLCONTEXT_H

#include "n-body/Core.h"
#include "n-body/Renderer/GraphicsContext.h"

struct GLFWwindow;

namespace NBody {

    class OpenGLContext: public GraphicsContext {
        public: 
            OpenGLContext(GLFWwindow* windowHandle);
            ~OpenGLContext();

            virtual void Init() override;
            virtual void SwapBuffers() override;

        private:
            GLFWwindow* m_windowHandle;
    };

}

#endif //OPENGLCONTEXT_H