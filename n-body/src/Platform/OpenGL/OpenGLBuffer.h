#ifndef OPENGLBUFFER_H
#define OPENGLBUFFER_H

#include <cstdint>
#include <cstring>

#include "n-body/Renderer/Buffer.h" 
#include <glad/glad.h>

namespace NBody {

    static GLenum OpenGLPixelFormat(PixelFormat type) {
        switch (type) {
            case PixelFormat::Red:	    return GL_RED;
            case PixelFormat::Green:	return GL_GREEN;
            case PixelFormat::Blue:	    return GL_BLUE;
            case PixelFormat::RGB:	    return GL_RGB;
            case PixelFormat::BGR:		return GL_BGR;
            case PixelFormat::RGBA:	    return GL_RGBA;
            case PixelFormat::BGRA:	    return GL_BGRA;
        }

        return 0;
    }

    class OpenGLVertexBuffer : public VertexBuffer {
        public:
            OpenGLVertexBuffer(float* verices, uint32_t size);
            OpenGLVertexBuffer(double* verices, uint32_t size);
            virtual ~OpenGLVertexBuffer();

            virtual void Bind() const;
            virtual void Unbind() const;

            virtual void SetLayout(const BufferLayout& layout) { m_Layout = layout; }
            virtual const BufferLayout& GetLayout() const { return m_Layout; }
            virtual const uint32_t GetID() const {return m_RendererID;};

        private:
            uint32_t m_RendererID;
            BufferLayout m_Layout;

    };

    class OpenGLIndexBuffer : public IndexBuffer {
	public:
		OpenGLIndexBuffer(uint32_t* indices, uint32_t count);
		virtual ~OpenGLIndexBuffer();

		virtual void Bind() const override;
		virtual void Unbind() const override;

		virtual uint32_t GetCount() const { return m_Count; }

	private:
		uint32_t m_RendererID;
		uint32_t m_Count;
	};

    class OpenGLPixelBuffer : public PixelBuffer {
	public:
        OpenGLPixelBuffer(unsigned int width, unsigned int height, PixelFormat format);
        virtual ~OpenGLPixelBuffer();

        virtual void Bind() const;
        virtual void Unbind() const;
        virtual void ReadPixels() const;
        virtual void GetPixels(unsigned char* pixels) const;
        virtual const uint32_t GetID() const {return m_RendererID;};

    private:
        uint32_t m_RendererID;

        unsigned int Width, Height;
        PixelFormat ColorFormat;
        GLenum GLFormat;
	};
}

#endif //OPENGLBUFFER_H