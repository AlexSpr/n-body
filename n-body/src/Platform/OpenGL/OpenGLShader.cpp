
#include "OpenGLShader.h"

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>

namespace NBody {

    OpenGLShader::OpenGLShader(const std::string& vertexSrcPath, const std::string& fragmentSrcPath){

        std::string fullVertexShaderFilePath = shadersDefaultPath;
        fullVertexShaderFilePath.append(vertexSrcPath);

        std::ifstream vertexShaderFile;
        vertexShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
        std::string vertexShaderSource;
        try {
            vertexShaderFile.open(fullVertexShaderFilePath);

            vertexShaderSource = std::string(std::istreambuf_iterator<char>(vertexShaderFile), (std::istreambuf_iterator<char>()));
        } 
        catch(std::ifstream::failure e) {
            std::cout << "Error while trying to read Vertex Source file!!!" << std::endl;
        }
        const char *vertexShaderCode = vertexShaderSource.c_str();

        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderCode, NULL);
        glCompileShader(vertexShader);

        GLint isCompiled = 0;
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);

        if(isCompiled == GL_FALSE){
            GLint maxLength = 0;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
            char infoLog[maxLength];    //std::vector<GLchar> infoLog(maxLength);
            glGetShaderInfoLog(vertexShader, maxLength, &maxLength, infoLog);

            glDeleteShader(vertexShader);
            std::cout << "ERROR: VERTEX SHADER COMPILATION_FAILED\n" << infoLog << std::endl;
            return;
        }

        std::string fullFragmentShaderFilePath = shadersDefaultPath;
        fullFragmentShaderFilePath.append(fragmentSrcPath);

        std::ifstream fragmentShaderFile;
        fragmentShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
        std::string fragmentShaderSource;
        try {
            fragmentShaderFile.open(fullFragmentShaderFilePath);

            fragmentShaderSource = std::string(std::istreambuf_iterator<char>(fragmentShaderFile), (std::istreambuf_iterator<char>()));
        } 
        catch(std::ifstream::failure e) {
            std::cout << "Error while trying to read Fragment Source file!!!" << std::endl;
        }
        const char *fragmentShaderCode = fragmentShaderSource.c_str();

        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderCode, NULL);
        glCompileShader(fragmentShader);
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);

        if(isCompiled == GL_FALSE){

            GLint maxLength = 0;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);
            char infoLog[maxLength];    //std::vector<GLchar> infoLog(maxLength);
            glGetShaderInfoLog(vertexShader, maxLength, &maxLength, infoLog);

            glDeleteShader(vertexShader);
            std::cout << "ERROR: FRAGMENT SHADER COMPILATION_FAILED\n" << infoLog << std::endl;
            return;
        }

        m_RendererID = glCreateProgram();
        GLuint program = m_RendererID;

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);
        glLinkProgram(program);

        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);

        if (isLinked == GL_FALSE){

            GLint maxLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
            char infoLog[maxLength];    //std::vector<GLchar> infoLog(maxLength);
            glGetProgramInfoLog(program, maxLength, &maxLength, infoLog);

            glDeleteProgram(program);
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);

            std::cout << "ERROR: LINKING SHADERS FAILED\n" << infoLog << std::endl;
            return;
        }

        // Always detach shaders after a successful link.
        glDetachShader(program, vertexShader);
        glDetachShader(program, fragmentShader);
    }

    OpenGLShader::~OpenGLShader(){
        glDeleteProgram(m_RendererID);
    }

    void OpenGLShader::Bind() const {
        glUseProgram(m_RendererID);
    }

    void OpenGLShader::Unbind() const {
        glUseProgram(0);
    }

    void OpenGLShader::UploadUniformInt(const std::string & name, int value)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniform1i(location, value);
    }

    void OpenGLShader::UploadUniformFloat(const std::string & name, float value)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniform1f(location, value);
    }

    void OpenGLShader::UploadUniformFloat2(const std::string & name, const glm::vec2 & value)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniform2f(location, value.x, value.y);
    }

    void OpenGLShader::UploadUniformFloat3(const std::string & name, const glm::vec3 & value)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniform3f(location, value.x, value.y, value.z);
    }

    void OpenGLShader::UploadUniformFloat4(const std::string & name, const glm::vec4 & value)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniform4f(location, value.x, value.y, value.z, value.w);
    }

    void OpenGLShader::UploadUniformMat3(const std::string & name, const glm::mat3 & matrix)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    }

    void OpenGLShader::UploadUniformMat4(const std::string& name, const glm::mat4 & matrix)
    {
        GLint location = glGetUniformLocation(m_RendererID, name.c_str());
        glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    }

}