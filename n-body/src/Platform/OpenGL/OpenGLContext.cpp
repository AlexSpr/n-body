
#include "OpenGLContext.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace NBody {

    OpenGLContext::OpenGLContext(GLFWwindow* windowHandle) : m_windowHandle(windowHandle){}

    OpenGLContext::~OpenGLContext(){}

    void OpenGLContext::Init(){
        glfwMakeContextCurrent(m_windowHandle);

        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        {
            std::cout << "Failed to initialize GLAD" << std::endl;
            return;
        }

        std::cout << "Vendor: "     << glGetString(GL_VENDOR) << std::endl;
        std::cout << "Renderer: "   << glGetString(GL_RENDERER) << std::endl;
        std::cout << "Version: "    << glGetString(GL_VERSION) << std::endl;

    }

    void OpenGLContext::SwapBuffers(){
        glfwSwapBuffers(m_windowHandle);
    }

}