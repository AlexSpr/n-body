
#include "OpenGLRenderer.h"

#include "Platform/OpenGL/OpenGLShader.h"


namespace NBody {
        void OpenGLRenderer::SetClearColor(const glm::vec4& color){
            glClearColor(color.r, color.g, color.b, color.a);
        }

        void OpenGLRenderer::Clear(){
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        
        void OpenGLRenderer::Submit(const Shader* shader, const std::shared_ptr<VertexArray>& vertexArray, const glm::mat4& transformMatrix){
            shader->Bind();
            OpenGLShader* OGLshader = (OpenGLShader*)shader;
            OGLshader->UploadUniformMat4("u_ViewProjection", s_SceneData->ViewProjectionMatrix);
            OGLshader->UploadUniformMat4("u_Transform", transformMatrix);

            vertexArray->Bind();
            //std::dynamic_pointer_cast<OpenGLShader>(shader)->UploadUniformMat4("u_ViewProjection", s_SceneData->ViewProjectionMatrix);
		    //Oshader->UploadUniformMat4("u_Transform", transformMatrix);


        }

        void OpenGLRenderer::SetCamera(Camera* camera){
            s_SceneData = new SceneData();
            s_SceneData->ViewProjectionMatrix = camera->GetViewProjectionMatrix();
        }

        void OpenGLRenderer::framebuffer_size_callback(GLFWwindow* window, int width, int height){
            glViewport(width/10, height/10, width/1.2, height/1.2);
        }
        
}
/*
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(width/10, height/10, width/1.2, height/1.2);
}

int OpenGLRenderer::launchWindow(int width, int height){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "N-Body Simulation", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    //glViewport(100, 100, 1000, 1000);

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    return 1;
}
*/