#include "OpenGLBuffer.h"

//#include <glad/glad.h>
#include <iostream>
namespace NBody {

	///////////////////////////////////////////////////////////////////////
	//// Vertex Buffer ////////////////////////////////////////////////////

	OpenGLVertexBuffer::OpenGLVertexBuffer(float* vertices, uint32_t size)
	{
		glCreateBuffers(1, &m_RendererID);
		glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	}

	OpenGLVertexBuffer::OpenGLVertexBuffer(double* vertices, uint32_t size)
	{
		glCreateBuffers(1, &m_RendererID);
		glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer()
	{
		glDeleteBuffers(1, &m_RendererID);
	}

	void OpenGLVertexBuffer::Bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_RendererID);
	}

	void OpenGLVertexBuffer::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	//// Vertex Buffer ////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////
	//// Index Buffer /////////////////////////////////////////////////////

	OpenGLIndexBuffer::OpenGLIndexBuffer(uint32_t * indices, uint32_t count) : m_Count(count)
	{
		glCreateBuffers(1, &m_RendererID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), indices, GL_STATIC_DRAW);
	}

	OpenGLIndexBuffer::~OpenGLIndexBuffer()
	{
		glDeleteBuffers(1, &m_RendererID);
	}

	void OpenGLIndexBuffer::Bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_RendererID);
	}

	void OpenGLIndexBuffer::Unbind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	
	//// Index Buffer /////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////
	//// Pixel Buffer /////////////////////////////////////////////////////

	OpenGLPixelBuffer::OpenGLPixelBuffer(unsigned int width, unsigned int height, PixelFormat format) 
		: Width(width), Height(height), ColorFormat(format), GLFormat(OpenGLPixelFormat(format))
	{
		glCreateBuffers(1, &m_RendererID);
		glBindBuffer(GL_PIXEL_PACK_BUFFER, m_RendererID);
		glBufferData(GL_PIXEL_PACK_BUFFER, Width * Height * PixelFormatSize(ColorFormat), NULL, GL_STREAM_READ);
	}

	OpenGLPixelBuffer::~OpenGLPixelBuffer()
	{
		glDeleteBuffers(1, &m_RendererID);
	}

	void OpenGLPixelBuffer::Bind() const
	{
		glBindBuffer(GL_PIXEL_PACK_BUFFER, m_RendererID);
	}

	void OpenGLPixelBuffer::Unbind() const
	{
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
	}

	void OpenGLPixelBuffer::ReadPixels() const
	{
		glBindBuffer(GL_PIXEL_PACK_BUFFER, m_RendererID);
		glReadPixels(0, 0, Width, Height, GLFormat, GL_UNSIGNED_BYTE, 0);
	}

	void OpenGLPixelBuffer::GetPixels(unsigned char* pixels) const 
	{
		glBindBuffer(GL_PIXEL_PACK_BUFFER, m_RendererID);
		unsigned char* ptr = (unsigned char*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
		if (NULL != ptr) {
			memcpy(pixels, ptr, Width * Height * PixelFormatSize(ColorFormat));
			glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
		}
		else {
			std::cout << "Failed to map the buffer" << std::endl;
		}
	}

	//// Pixel Buffer /////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////

}