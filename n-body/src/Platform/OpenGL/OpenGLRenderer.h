#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include "n-body/Core.h"
#include "n-body/Window.h"

#include "n-body/Renderer/Camera.h"
#include "n-body/Renderer/Renderer.h"
#include "n-body/Renderer/VertexArray.h"
#include "n-body/Renderer/Shader.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace NBody {

    class OpenGLRenderer : public Renderer {
    public:
        OpenGLRenderer(){}

        void Submit(const Shader* shader, const std::shared_ptr<VertexArray>& vertexArray, const glm::mat4& transformMatrix = glm::mat4(1.0f));

        void framebuffer_size_callback(GLFWwindow* window, int width, int height);

        virtual void SetCamera(Camera* camera) override;

        virtual void SetClearColor(const glm::vec4& color) override;
        virtual void Clear() override;
    private:
        struct SceneData {
			glm::mat4 ViewProjectionMatrix;
		};

		SceneData* s_SceneData;
    };

}

#endif //OPENGLRENDERER_H