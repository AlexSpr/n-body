#!/bin/sh
tput reset
echo "compiling nbody problem"
cmake3 .
make
echo "starting simulation"
./bin/linux/n-body 
#pkill -f plotSim.py
exit 1
