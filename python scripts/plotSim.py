import numpy as np
import random
import argparse

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex
import matplotlib.animation as animation


n = 4
radius = 1000
coordinates = np.zeros((n,3))
file = 'body_coordinates.txt'


def set_coordinate(coordinate_array):
    t = 2*np.pi*random.random()
    u = random.random()+random.random()
    if u>1:
        r = 2 - u
    else:
        r = u
    #print(r,u)    
    coordinate_array[0] = (r*np.cos(t))    
    coordinate_array[1] = (r*np.sin(t))
    rand_dist = np.sqrt(coordinate_array[0]**2 + coordinate_array[1]**2) 
    pos_possibility = np.sqrt(np.sin(rand_dist**4) - rand_dist**2 * np.sin(rand_dist**4))/np.sqrt(5)
    coordinate_array[2] =  random.uniform( -pos_possibility, pos_possibility)#coordinate_array[0]**2 + coordinate_array[1]**2#random.uniform( 0, (np.sin(rand_dist**3 * np.pi)) * .12 * z)

    coordinate_array[0] *= radius   
    coordinate_array[1] *= radius
    coordinate_array[2] *= radius 


class PLOT():
    def __init__(self, n, sim_radius, input_file):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.input_file = input_file

        self.sim_radius = sim_radius
        self.sim_step_counter = -1
        self.colors_set = False
        self.body_colors = None

        self.coordinates = None


        self.color_maps = ['viridis', 'binary', 'magma', 'inferno', 'gist_heat', 'cool', 'autumn']
        self.fig.canvas.set_window_title('n-body problem')
        #self.ax.autoscale(False)
        self.ax.set_xlim([-self.sim_radius, self.sim_radius])
        self.ax.set_ylim([-self.sim_radius, self.sim_radius])
        self.ax.set_zlim([-self.sim_radius/2, self.sim_radius/2])
        #self.ax.autoscale(False)
        #plt.axis('equal')

        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_zlabel('z')

    def set_body_colors(self, initial_coordinates):
        color_gradient = np.asarray([ np.sqrt(x**2 + y**2 + z**2) for x,y,z in zip(initial_coordinates[:, 0], initial_coordinates[:, 1], initial_coordinates[:, 2]) ]) / radius
        color_cm = plt.cm.get_cmap(self.color_maps[3],256)
        self.body_colors = color_cm(color_gradient)

    def plot(self, coordinates_array): 
        self.ax.set_xlim([-self.sim_radius, self.sim_radius])
        self.ax.set_ylim([-self.sim_radius, self.sim_radius])
        self.ax.set_zlim([-self.sim_radius/2, self.sim_radius/2])

        self.ax.scatter(coordinates_array[:,0], coordinates_array[:,1], coordinates_array[:,2], c= self.body_colors , norm = None, alpha=0.85)

    def show(self):
        plt.show()

    def animate(self, i):
        graph_data = open(file, 'r').read()
        lines = graph_data.split('\n')
        for line in lines:
            if len(line) > 1:
                try:
                    split_line = [float(x) for x in line.split(',')]
                except:
                    split_line = [-1]
                #print(len(split_line[1:]))
                if split_line[0] > self.sim_step_counter and len(split_line[1:n * 3 + 1]) == n * 3:
                    self.sim_step_counter = split_line[0]
                    
                    self.coordinates = np.reshape(split_line[1:n * 3 + 1], (-1,3))
                    #print(coordinates.shape)
                    if not self.colors_set:
                        self.set_body_colors(self.coordinates)
                        self.colors_set = True

                    self.ax.clear()
                    self.plot(self.coordinates)
        #return self.coordinates

if "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-test', action = "store_true" , help = 'tests function created in plotSim', required = False, default = False)
    
    args = parser.parse_args()
    to_test = args.test

    simPlot = PLOT(n, radius, file)

    if to_test:
        for i in range(len(coordinates)):
            set_coordinate(coordinates[i])
        simPlot.set_body_colors(coordinates)
        simPlot.plot(coordinates)
        simPlot.show()

    else:
        open(file, 'w').close()
        ani = animation.FuncAnimation(simPlot.fig, simPlot.animate, interval = 200)
        simPlot.show()
