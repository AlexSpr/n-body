import numpy as np 
import sys

if __name__ == "__main__":
    n = int(sys.argv[1])
    
    bytes_multiplers = ["", "kilo", "mega", "giga", "tera", "peta", "exa", "zetta", "yota"]

    value_type = 8
    coordinates_mem_size = n * 3 * value_type
    velocities_mem_size = n * 3 * value_type
    masses_mem_size = n * value_type

    distanceMag_mem_size = n * n * value_type
    gravVec_mem_size = n * n * 3 * value_type

    total_needed_mem = coordinates_mem_size + velocities_mem_size + masses_mem_size + distanceMag_mem_size + gravVec_mem_size

    bytes_size = 0
    while ((total_needed_mem ) > 1000): #(10 ** (3 * bytes_size)

        
        total_needed_mem /= 1000
        bytes_size += 1
    print("Print Total Mem:", total_needed_mem, bytes_multiplers[bytes_size] + " bytes")


    print("Distance Calc:")
    distance_mem = coordinates_mem_size + distanceMag_mem_size + gravVec_mem_size
    bytes_size = 0
    while ((distance_mem ) > 1000): #(10 ** (3 * bytes_size)
        distance_mem /= 1000
        bytes_size += 1


    print("Print Distance Mem:", distance_mem, bytes_multiplers[bytes_size] + " bytes")



    print("\n\nALTERNATIVE STORAGE METHOD")

    value_type = 8
    coordinates_mem_size = n * 3 * value_type
    velocities_mem_size = n * 3 * value_type
    masses_mem_size = n * value_type

    distanceMag_mem_size = (n - 1) * n / 2* value_type
    gravVec_mem_size = (n - 1) * n * 3 / 2* value_type

    total_needed_mem = coordinates_mem_size + velocities_mem_size + masses_mem_size + distanceMag_mem_size + gravVec_mem_size

    bytes_size = 0
    while ((total_needed_mem ) > 1000): #(10 ** (3 * bytes_size)

        
        total_needed_mem /= 1000
        bytes_size += 1
    print("Print Total Mem:", total_needed_mem, bytes_multiplers[bytes_size] + " bytes")


    print("Distance Calc:")
    distance_mem = coordinates_mem_size + distanceMag_mem_size + gravVec_mem_size
    bytes_size = 0
    while ((distance_mem ) > 1000): #(10 ** (3 * bytes_size)
        distance_mem /= 1000
        bytes_size += 1

    print("Print Distance Mem:", distance_mem, bytes_multiplers[bytes_size] + " bytes")