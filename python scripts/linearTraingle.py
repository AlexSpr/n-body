import numpy as np
import sys
import time

if __name__ == "__main__":
    n = int(sys.argv[1])
    #index = int(sys.argv[2])
    nrange = int((n-1)*n/2)

    proven_row_values = np.zeros(nrange)
    proven_column_values = np.zeros(nrange)
    exp_row_values = np.zeros(nrange)
    exp_column_values = np.zeros(nrange)


    
    print("proven func")
    start = time.time()
    counter = 0
    mean_duration_time = 0
    for i in range(n):
        #print("i",i)
        for j in range(i):
            #print("counter",counter)
            duration_start = time.time()
            proven_row_values[counter] = i
            proven_column_values[counter] = j
            counter += 1
            duration_end = time.time()
            mean_duration_time += duration_end - duration_start

    end = time.time()

    print("Compute time (sec): {}".format(end - start))
    print("Mean step time (sec): {}".format(mean_duration_time/counter))
    '''
    for index in range(nrange):
        i = n - 2 - np.floor(np.sqrt(-8*index + 4*n*(n-1)-7)/2.0 - 0.5)
        j = index + i + 1 - n*(n-1)/2 + (n-i)*((n-i)-1)/2
        #j = -i * n * (i/2 + 3/2)*i + index +1 
        proven_row_values[index] = i
        proven_column_values[index] = j
        print("column:", i,"row:",j)
    '''
    #print("x:", i+1,"y:",j+1)
    
    print("experimental func")
    #m = n - 1
    mean_duration_time = 0
    start = time.time()
    for index in range(nrange):
        duration_start = time.time()
        row = int((-1+np.sqrt(8 * index + 1))/2)
        column = int(index - row * (row+1)/2)

        row = row + 1
        #column = column

        exp_row_values[index] = row 
        exp_column_values[index] = column
        duration_end = time.time()
        mean_duration_time += duration_end - duration_start
        #print("column:", y,"row:",x)
        #print("i:", i,"j:",j,"k:",k)
    end = time.time()
    print("Compute time (sec): {}".format(end - start))
    print("Mean step time (sec): {}".format(mean_duration_time/counter))

    print("\nNumber of caclulations:", nrange)
    if (np.array_equal(exp_row_values, proven_row_values) and np.array_equal(exp_column_values, proven_column_values)):
        print("The experimental function calculates correctly!!!!!")
    else:
        print("The experimental function is NOT working")

    print("EXP row:\t", exp_row_values)
    print("EXP column:\t", exp_column_values)
    print()
    print("PROVEN row:\t", proven_row_values)
    print("PROVEN column:\t", proven_column_values)
