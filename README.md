# N-Body Simulation using OpenGL and OpenCL

![](readmemedia/n-body-zoom.png)

## Requirements 

### Hardware and Software
- Linux (tested on CentOS)
- OpenGL capable Hardware (only tested on Nvidia GPU)

- OpenCL (OpenCL may have to be manually installed depending on the Hardware)
- CMake3

### Manual Changes to Files that are Required (for now to make it work)

#### In CmakeLists.txt;
the OpenCL link is currently hard coded (1)

    #OPENCL
    add_library(OpenCL SHARED IMPORTED) # set opencl library location to project
    set_target_properties(OpenCL PROPERTIES
      IMPORTED_LOCATION "/usr/local/cuda-10.1/targets/x86_64-linux/lib/libOpenCL.so"
      INTERFACE_INCLUDE_DIRECTORIES "/usr/local/cuda-10.1/targets/x86_64-linux/include"
    )
    
and linked at the end of the file with the executable (2); 

    target_link_libraries(${PROJECT_NAME} OpenCL glfw glad glm)
    
Since the OpenCL location will be different depending on the hardware vendor,
try linking it automatically by replacing (1) with one of the methods described here; 
- https://streamhpc.com/blog/2015-09-25/handling-opencl-with-cmake-3-1-and-higher/

If automatic linking does not working find the OpenCL library path and replace the
two paths in (1)

#### If one uses a CPU to run the simulation 

replace line 17 in n-body/n-body/src/n-body/opencl/openclhelper.cpp

    default_platform.getDevices(CL_DEVICE_TYPE_GPU, &all_devices);
    
with 

    default_platform.getDevices(CL_DEVICE_TYPE_CPU, &all_devices);
    
This might still lead to a crash due to how OpenCL scripts are currently written

### Simulation Variables in n-body/n-body/src/n-body/nbody.cpp

1. int n = 10000;  

    Amount of bodies to be simulated 

2. int nDefined = 3;
    
    Amount of Bodies which features are defined by the user 

3. int nStatic = 1;
    
    Amount of Bodies defined to be static. Their position will not change in the simulation 

4. nbodyvar simRadius = 1000;

    The max radius, where bodies can first "spawn" at when initializing the simulation

5. int timeSteps = 100000000;

    The amount of time steps in the simulation 

6. float dt = 0.1;

    Delta time 
    
Depending on how many nDefined bodies are set one has to add these lines:

    //SUN
    coordinatesArray[0] = 0;
    coordinatesArray[1] = 0;
    coordinatesArray[2] = 0;
    velocitiesArray[0] = 0;
    velocitiesArray[1] = 0;
    velocitiesArray[2] = 0;
    massesArray[0] = 1000;
    
    //JUPITER
    coordinatesArray[3] = 300; // 0.00541640576 * 1000
    coordinatesArray[4] = 0;
    coordinatesArray[5] = 0;
    massesArray[1] = 1;
    
    //SATURN
    coordinatesArray[6] = 600; // 0.00997704028 * 1000
    coordinatesArray[7] = 0;
    coordinatesArray[8] = 0;
    massesArray[2] = 0.28571428571;


### Simulation Variables in n-body/n-body/src/n-body/nbody.h

1. #define PI          3.141592653589793238462643383279502884L /* pi */
2. #define G           1.0//6.67300E-11 /* gravitational constant */
    
    Strength of Gravity in Simulation 

3. #define MASS_MIN    0.0000000001//0.001
    
    Minimum mass of body

4. #define MASS_MAX    0.00000001
    
    Maximum Mass of body 



    
## Compiling and Running the Simulation 
Executing CompileandRunSim.sh will compile and run the simulation. The simulation executable 
is compiled in a newly created bin/ directory. 

To manually compile and run the n-body sim, enter the commands in the CompileandRunSim.sh
file into the command line one by one. 

